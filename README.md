DevOps Pairing Exercise
=======================

# Goal

Automate the creation of a web server, and provide a healthcheck script to verify the server is up and responding correctly.

# The Task

You are required to provision and deploy a new service in AWS. It must:

* Run on EKS
* Be publicly accessible, but *only* on port 80.
* Run Nginx.
* Make the service resilient in 3 availability zones.
* Serve a `/version.txt` file, containing only static text representing a version number, for example:
